<?php

use Buzz\Browser;
use Buzz\Client\Curl;

return function() {
  new Browser(new Curl());
};
