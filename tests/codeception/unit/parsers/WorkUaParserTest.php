<?php

namespace tests\codeception\unit\parsers;

use app\parsers\WorkUaParser;

require_once __DIR__.'/../../../../parser/WorkUaParser.php';

class WorkUaParserTest extends \Codeception\TestCase\Test
{
    /**
     * @var \UnitTester
     */
    protected $tester;

    protected function _before()
    {
    }

    protected function _after()
    {
    }

    protected function getResponseMock($content)
    {
        $response = $this->getMock('Buzz\Message\MessageInterface');
        $response->expects($this->once())
            ->method('getContent')
            ->will($this->returnValue($content));

        return $response;
    }

    public function testCollect()
    {
        $response = $this->getResponseMock(file_get_contents(__DIR__.'/php-vinnitsa.html'));

        $browser = $this->getMock('Buzz\Browser');
        $browser->method('get')
            ->with('http://www.work.ua/jobs-vinnytsya-php/')
            ->will($this->returnValue($response));

        $parser = new WorkUaParser($browser);
        $urls = $parser->collectUrls('http://www.work.ua/jobs-vinnytsya-php/');
        $this->assertEquals([
          'http://www.work.ua/jobs/1849408/',
          'http://www.work.ua/jobs/1849391/',
          'http://www.work.ua/jobs/1706948/',
          'http://www.work.ua/jobs/1866570/',
          'http://www.work.ua/jobs/1883203/',
      ], $urls);
    }

    public function testParse()
    {
      $response = $this->getResponseMock(file_get_contents(__DIR__ . '/1706948.html'));

      $browser = $this->getMock('Buzz\Browser');
      $browser->method('get')
          ->with('http://www.work.ua/jobs/1849408/')
          ->will($this->returnValue($response));

      $parser = new WorkUaParser($browser);
      $data = $parser->parse('http://www.work.ua/jobs/1849408/');

      $this->assertEquals('PHP-разработчик', $data['title']);
      $this->assertEquals('TechFunder', $data['company']);
      $this->assertEquals('Винница', $data['city']);
    }

    public function testParseSalary()
    {
      $response = $this->getResponseMock(file_get_contents(__DIR__ . '/1124502.html'));

      $browser = $this->getMock('Buzz\Browser');
      $browser->method('get')
          ->with('http://www.work.ua/jobs/1124502/')
          ->will($this->returnValue($response));

      $parser = new WorkUaParser($browser);
      $data = $parser->parse('http://www.work.ua/jobs/1124502/');

      $this->assertEquals(8000, $data['salary']);
    }
}
