<?php

namespace app\parsers;

use Buzz\Browser;
use Symfony\Component\DomCrawler\Crawler;

class WorkUaParser
{
    private $browser;

    public function __construct(Browser $browser)
    {
        $this->browser = $browser;
    }

    public function collectUrls($url)
    {
        $content = $this->browser->get($url)->getContent();
        $crawler = new Crawler($content, $url);

        return $crawler->filter('a.sj-j')->each(function (Crawler $el) {
            return $el->link()->getUri();
        });
    }

    public function parse($url)
    {
        $content = $this->browser->get($url)->getContent();
        $crawler = new Crawler($content, $url);

        $salary = null;
        $salaryEl = $crawler->filter('h3 b');
        if (count($salaryEl)) {
            $salary = $salaryEl->text();
        }

        return [
        'title' => $crawler->filter('h1')->text(),
        'company' => $this->selectFromTable($crawler, 'Компания'),
        'city' => $this->selectFromTable($crawler, 'Город'),
        'salary' => $salary,
      ];
    }

    private function selectFromTable(Crawler $crawler, $label)
    {
        return trim($crawler->filter(".dl-horizontal dt:contains('$label') + dd")->text());
    }
}
